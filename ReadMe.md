# Streamer 3.0 的開發

## Feature
可讀json格式的config檔, 來指揮Golang寫的parser的動作

# Change Log
==========================

- 0.1 
    - [0.1.1]
        - 可讀 json 
        - 可 load 進 .so 檔
    - [0.1.2]
        - 可讀 csv
    - [0.1.3]
        - 可讀url, 以及url裡包含的json
        - config 不是從flag讀, 而是掃描檔名為 "_config.json"結尾的檔
        - 待改進:  
            - source_json.go, source_csv.go, source_url.go三者其實可以合併的function:
                Get____Struct, initial_struct, get_item, get_itemtype
        - 修正問題: 
            - "config沒寫的欄位, 會被塞 bofield_content[0]" 的問題, 改為 如果config沒寫則塞空白
            - 修改 agent_test_xxx_streamer_3.conf 的 kafka_group, 讓 3 支 streamer可以並行同時收資料
    - [0.1.4]
        - 整合, 刪除掉Source_Json, Source_Csv, Source_Url 這三個type, 改成用一個All_Version_Struct (因為三者做的事其實是一樣的)
        - 刪除 field_key 這個變數
        - 加入 List_redundant_fields(): 列出config沒有定義的欄位
    - [0.1.5]
        - 修正原本無法寫入多個table的bug: 修改 assign 空間給 boColOrder[ver] 和 colData[ver]的位置, 否則前一個table的內容會被後面的table洗掉
        - 修正 row_success不夠完整的bug: 原本一筆資料如果parse到一半失敗, 而又要寫進多個table, 有可能會只寫parse成功的table. 現在修改成: 要確定整筆資料都parse成功, 才一次寫進所有table.
        - 當row_success === false時, 確定有error message才寫進fatal_error
        - 加入put_outer_data_in_json(): 為了gamania 把 ip 和時間 塞入json中
- 0.2
    - [0.2.1]
        - 加了 bo_fields_map: 為了在 parsing 的時候, 可以使用別的欄位 parsing 的結果, 所以必須要把整個 bo_fields 傳給.so檔. 
            而因為裡面有 BO_FIELD, 所以要先把它轉成 map
            - 為了轉出 bo_fields_map, 把 BO_FIELD 的欄位全部改成大寫
        - 加了 Parse_result_cache: 可以把parsing的結果, 或parsing後需要被別的欄位使用的結果 存起來
        - parse_func 和 傳給 .so 檔的 parameter 改變: 加了 table, key, 方便可以任意存取 bo_fields_map 和 Parse_result_cache
        - 修正 parse_func()裡 : 找到 function name 就 break出去
    - [0.2.2]
        - 已經可以收 gamania 的部分正常json格式資料, 但json裡的co裡包含陣列的還不行
        - 修改了 run_json_stramer_3.sh, run_csv_stramer_3.sh, run_url_stramer_3.sh
            加了 export LD_LIBRARY_PATH=$(pwd), 這樣就不必每次跑streamer都要執行 export LD_LIBRARY_PATH= OOXX
        - 修正問題: 原本某些欄位不直接寫進 BO, 但可能會是其他欄位的input. 這種欄位會定義_bo, 但不會定義 table_setting.
            解法: 在建構 boCol 和 bo_fields 時, 將這些欄位的 Order 設為負值
        - 修正bug: colData 在多thread時會衝突
        - 在config加了 list_redundant_fields
        - 新增一個 datasource.source_type 為 "gamania_json"
    - [0.2.3]
        - 修改 WriteToFile(): 因為把streamer包進docker執行之後, 碰到file system不支援 open() 裡的 flag: O_TMPFILE, 要另找方法開tempfile
                             目前是用 ioutil.TempFile()
    - [0.2.4]
        - 新增 write_to_influxdb.go: streamer 可以將資料寫入influxdb.
            這裡雖然有些 function 和全域變數 的功能 和 原本的 streamer 類似, 但為了讓普通 streamer 也能正常compiler, 所以刻意取不同的名字, ex: loadconfig_influxdb, config_setting_for_influxdb()
        - 修改 load_plugin() 和 config_setting(): 沒讀到 file 的時候不要 panic 當掉 
        - 修正問題: 在未讀到任何 .so檔的情況下, 就先讀到 go_json2csv.so, 就會當掉.
            fatal error: runtime: no plugin module data
            解法: 修改 load_plugin():  go_開頭 且 .so結尾的檔 略過. (也就是不讀 go_json2csv.so) 
    - [0.2.5]
        - 修了粗心 bug: temp_tables_rows[wstbl] = append(tables_rows[wstbl], row) 改成 temp_tables_rows[wstbl] = append(temp_tables_rows[wstbl], row)
		- 為了 influxdb 新增了 table_key_list, 和 loadconfig.Table_Key
        - 配合 streamer_c [0.0.6] 修改: 
            - 修改 Makefile: 
                - 為了要讓 influxdb_transmitter.cpp 能抓到 go_json2csv.h, 改成 $(GOCONVERTER) 比 base 先執行.
                - 並為了build shared object 新增 shared_object
                        
                    綜合以上兩點    --> all: shared_object $(GOCONVERTER) base $(SAGENTPROG)
                    
                - 把 cgo 產生的 go_json2csv.h 複製到 include 資料夾底下 --> cp go_$(GOCONVERTER).h ../$(INCDIR)/go_json2csv.h; \
                - make clean 時新增了 clean_go, clean_shared_object

        - Comma 可以 從 json 讀進來
        - source_csv.go:CsvUnmarshal(): 把欄位數目不同時的 success = false 刪掉, 並在下方的 for 加了條件 i < len(record)
    - [0.2.6]
        - 修改 write_to_influxdb.go: 一個 batch 可以包 5000個 point 才 write 一次, 速度快了 40 倍 (但是總傳輸量超過大約 6.7MB 左右會爆掉, 詳細看 trello
    - [0.2.7]
        - 新增 c_functions.go (Makefile 也要改): Kevin 發現, 原本用的 ioutil.WriteFile() 在多線 goroutine 時, 會產生重複的 fd, 所以 改用 C++ 的 tmpfile() 來產生 tempfile