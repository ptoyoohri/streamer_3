#!/bin/bash

export GODEBUG=cgocheck=0
export LD_LIBRARY_PATH=$(pwd)

if [ ! -f /home/functions.so ]; then
    cp -a /home/exec_file/functions.so /home/functions.so
fi
if [ ! -f /home/go_json2csv.so ]; then
    cp -a /home/exec_file/go_json2csv.so /home/go_json2csv.so
fi
if [ ! -f /home/sagent_streamer_3 ]; then
    cp -a /home/exec_file/sagent_streamer_3 /home/sagent_streamer_3
fi

while [ "1" == "1" ];
do
    echo "run streamer"
    ./sagent_streamer_3 agent_streamer_3_influxdb.conf influxdb
    if [[ $? -ne 101 ]]; then
        exit
    fi
    echo "Restart streamer after 1 minute..."
    sleep 60
done
