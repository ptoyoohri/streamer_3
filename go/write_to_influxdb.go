// $$ reference to https://github.com/sanathp/statusok/blob/master/database/influxdb.go
package main

import (
	"C"
	"bytes"
	"encoding/csv"
	"fmt"
	"strconv"
	"sync"
	"syscall"
	"time"
	"os"
	_ "github.com/influxdata/influxdb1-client" // this is important because of the bug in go mod
	client "github.com/influxdata/influxdb1-client/v2"
)

type InfluxDB_Client_struct struct {
	InfluxDB_Client client.Client
	Username        string
	Password        string
	Address         string
	Port            string
	Workspace       string
}

//export CsvToInfluxDB
func CsvToInfluxDB(addr string, port string, username string, password string, workspace string, table_name string, inFd []int) (success bool, err_str []string) {

	success = true
	err_str = make([]string, len(inFd))

	var wg sync.WaitGroup

	for idx, jFd := range inFd { // $$ 一個table (csv file)
		wg.Add(1)
		go func(idx, jFd int) {
			var connection InfluxDB_Client_struct
			defer func() {
				if connection.InfluxDB_Client != nil {
					connection.InfluxDB_Client.Close() // $$ close connection
				}

				wg.Done()
			}()
			//=======================

			var inStat syscall.Stat_t
			if err := syscall.Fstat(jFd, &inStat); err != nil {

				err_str[idx] = "Get input file state fail: " + err.Error()
				return
			}

			//	fmt.Println("//==========================", jFd)
			source_data, err := syscall.Mmap(jFd, 0, int(inStat.Size), syscall.PROT_READ, syscall.MAP_SHARED)
			if err != nil {
				success = false
				err_str[idx] = "Mmap input file fail: " + err.Error()
				return // success, err_str
			}

			defer syscall.Munmap(source_data)
			//=============================================
			var reader = csv.NewReader(bytes.NewReader(source_data))
			//	reader.Comma = '\t'                  // $$ 設定分隔字元

			reader.Comma = ','
			//	fmt.Println("reader.Comma: ", reader.Comma)
			reader.Comment = '#'                 // $$ 設定註解字元
			csv_records, err := reader.ReadAll() // $$ 一次讀完整個csv
			if err != nil {
				success = false
				err_str[idx] = "Read csv fail: " + err.Error()
				return // success, err_str
			}
			//	fmt.Println("csv_records: ", csv_records)
			// ?? pre_source_cnt的功能還沒加
			//============================================
			//	table := table_list[idx]
			//	table_temp := strings.Split(table, ".")
			//	fmt.Println("table: ", table, ", fd: ", jFd)
			//	workspace := // table_temp[0]
			//	table_name := table_list[idx]
			//	fmt.Println(table_list[idx], ", ", table_name)

			// $$ to be safe, open a connection for each thread, even though seems like it is OK to multithreading in one connection
			connection.Address = "http://" + addr // "http://127.0.0.1"
			connection.Port = port                // "8086"
			connection.Username = username        // "monitor"
			connection.Password = password        // "secret"
			connection.Workspace = workspace

			connect_bool, connect_err := connection.Build_influxdb_connection() // $$ 開啟influxdb的connection
			if !connect_bool {
				success = false
				err_str[idx] = connect_err.Error()
				return
			}

			if create_bool, create_err := connection.Create_database(); !create_bool {
				success = false
				err_str[idx] = create_err.Error()
				return //success, err_str
			}

			//	success_count := 0
		//	fmt.Println("jFd: ", jFd)
			/*
			for i, row := range csv_records {
			//	fmt.Println(i, ", ", row)
				if i % 10000 == 0 {
					connection.get_total_count(table_name)
				}
				if compose_bool, compose_err := connection.Add_to_InfluxDB(row, table_name, jFd, i); !compose_bool {
					//	fmt.Println("compose_err: ", compose_err)
					err_str[idx] = fmt.Sprintf("row %d: %v;", i , compose_err)//"row " + to_string(i) + ": " + compose_err.Error() + "; "
				}
			}
			*/
			if compose_bool, compose_err := connection.Add_to_InfluxDB_2(csv_records, table_name); !compose_bool {
				//	fmt.Println("compose_err: ", compose_err)
				err_str[idx] = fmt.Sprintf("write %s: %v;", table_name , compose_err)//"row " + to_string(i) + ": " + compose_err.Error() + "; "
			}
			//	fmt.Println("write to " + connection.Workspace + "." + table_name + ": " + strconv.Itoa(success_count) + " records")
			//=========================
			//	syscall.Munmap(source_data)
			
			
		}(idx, jFd)
		wg.Wait()
	}

	var Now_Time_mili int64 = time.Now().UTC().UnixNano() / 1000000
	fmt.Println("write to " + workspace + "." + table_name + " finished: " + time.Now().Format("2006-01-02 15:04:05") + "\t" + fmt.Sprintf("%d", Now_Time_mili) )

	return success, err_str
}

func (connection *InfluxDB_Client_struct) Build_influxdb_connection() (bool, error) {
	var err error
	connection.InfluxDB_Client, err = client.NewHTTPClient(client.HTTPConfig{
		Addr:     connection.Address + ":" + connection.Port, //"http://127.0.0.1:8086",
		Username: connection.Username,                        // ?? Leo: 幹嘛用的?
		Password: connection.Password,
	})
	if err != nil {
		return false, fmt.Errorf("Build_influxdb_connection fail: %s:%s, USR/PWD: %s / %s; %v", connection.Address, connection.Port, connection.Username, connection.Password, err) // err
	}

	return true, err
}

func (connection *InfluxDB_Client_struct) Add_to_InfluxDB(record []string, table_name string, fd int, row_number int) (bool, error) {
	success := true

	bp, err := client.NewBatchPoints(client.BatchPointsConfig{
		Database:  connection.Workspace,
		Precision: "s", // ?? Leo: What's this?
	})
	if err != nil {
		success = false
		return success, fmt.Errorf("client.NewBatchPoints: %v", err)
	}

	if _, exist := loadconfig.TableSetting[connection.Workspace+"."+table_name]; !exist {
		//do something here
		success = false
		return success, fmt.Errorf(connection.Workspace + "." + table_name + " does not exist in the config.json")
	}

	fields := make(map[string]interface{})
	tags := make(map[string]string)

	for i, value := range record { // $$ 建構 fields

		// $$ 不知道為什麼 record 有時候後面會多一個空白欄位, 要設一個break以防萬一
		if i >= len(loadconfig.TableSetting[connection.Workspace+"."+table_name].([]interface{})) {
			//	fmt.Println("out of range")
			break
		}

		field_name := loadconfig.TableSetting[connection.Workspace+"."+table_name].([]interface{})[i].(string)
		//	fmt.Println(field_name + ": " + value)

		if _, is_tag := table_key_list[connection.Workspace+"."+table_name][field_name]; is_tag {
			tags[field_name] = value
		} else {
			fields[field_name] = value
		}

	}
	//	fmt.Println("fields: ", fields)
	//	fmt.Println("tags: ", tags)

	var eventTime time.Time
	eventTime = time.Now() //.Add(time.Second * -20)
	//=================================================
	/*

		var record_limit int = 100000
		var serial_number int
		for serial_number = 0; serial_number < record_limit; serial_number++ {
			tags["uniq"] = strconv.Itoa(fd) + "_" + strconv.Itoa(row_number) + "_" + strconv.Itoa(serial_number)
			eventTime = time.Now() //.Add(time.Second * -20)
			check_suc, check_exist, _ := connection.Check_Record_Exist(table_name, eventTime, tags, fields)
			if !check_suc || !check_exist {
				break
			}
		}

		if serial_number >= record_limit {
			success = false
			return success, fmt.Errorf("records exceed limit %v at: eventTime: %v, fd: %v, row_number: %v", record_limit, eventTime, fd, row_number)
		}
	*/
	/*
		var eventTime time.Time
		eventTime = time.Now()
		tags["uniq"] = strconv.Itoa(fd) + "_" + strconv.Itoa(row_number)
	*/
	//=================================================
	point, err := client.NewPoint(
		table_name,
		tags,
		fields,
		eventTime, //.Add(time.Second*10),
	)
	//	fmt.Println("point: ", point)
	if err != nil {
		success = false
		return success, fmt.Errorf("client.NewPoint: %v", err)
	}

	bp.AddPoint(point)
	err = connection.InfluxDB_Client.Write(bp)
	if err != nil {
		success = false

		return success, fmt.Errorf("connection.InfluxDB_Client.Write: %v", err)
	}

	return success, err
}



func (connection *InfluxDB_Client_struct) Add_to_InfluxDB_2(csv_records [][]string, table_name string) (bool, error) {
	success := true

	bp, err := client.NewBatchPoints(client.BatchPointsConfig{
		Database:  connection.Workspace,
		Precision: "s", // ?? Leo: What's this?
	})
	
	if err != nil {
		success = false
		return success, fmt.Errorf("client.NewBatchPoints: %v", err)
	}

	if _, exist := loadconfig.TableSetting[connection.Workspace+"."+table_name]; !exist {
		//do something here
		success = false
		return success, fmt.Errorf(connection.Workspace + "." + table_name + " does not exist in the config.json")
	}

	

	var point_err_str string
	row_number := len(csv_records)
	last_write_row := 0
	for j, record := range csv_records { // $$ j will be 0 ~ row_number
		fields := make(map[string]interface{})
		tags := make(map[string]string)

		for i, value := range record { // $$ 建構 fields
			
			// $$ 不知道為什麼 record 有時候後面會多一個空白欄位, 要設一個break以防萬一
			if i >= len(loadconfig.TableSetting[connection.Workspace+"."+table_name].([]interface{})) {
				//	fmt.Println("out of range")
				break
			}
	
			field_name := loadconfig.TableSetting[connection.Workspace+"."+table_name].([]interface{})[i].(string)
			//	fmt.Println(field_name + ": " + value)
	
			if _, is_tag := table_key_list[connection.Workspace+"."+table_name][field_name]; is_tag {
				tags[field_name] = value
			
			} else {
				fields[field_name] = value
			
			}
		}
		var eventTime time.Time
		eventTime = time.Now() //.Add(time.Second * -20)

		point, NewPoint_err := client.NewPoint(
			table_name,
			tags,
			fields,
			eventTime, //.Add(time.Second*10),
		)
		//	fmt.Println("point: ", point)
		if NewPoint_err != nil {
			success = false
			point_err_str = point_err_str + fmt.Sprintf("row %d err: %v; ", j, NewPoint_err)
		
		}
	
		bp.AddPoint(point)

		if (j > 0 && j % 5000 == 0) || (j >= (row_number - 1) ) { // $$ Write every 5000 points ||  reach the limit so write the rest records

			if Write_err := connection.InfluxDB_Client.Write(bp); Write_err != nil {
				success = false
				point_err_str = point_err_str + fmt.Sprintf("Write err on row %d ~ %d: %v; ", last_write_row, j, Write_err)
			} else {
			//	connection.get_total_count(table_name)
				message := fmt.Sprintf("%v\trows\t%v\n", time.Now().UnixNano() / int64(time.Millisecond), j)
				f, err := os.OpenFile("StressTesting.txt", os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
				if err != nil {
					panic(err)
				}

				defer f.Close()

				if _, err = f.WriteString(message); err != nil {
					panic(err)
				}
			}
			last_write_row = j
		} 
	}

	/*
	err = connection.InfluxDB_Client.Write(bp)
	if err != nil {
		success = false

		return success, fmt.Errorf("connection.InfluxDB_Client.Write: %v", err)
	}
*/
	if len(point_err_str) > 0 {
		err = fmt.Errorf("%v", point_err_str)
	}

	return success, err

	
	//=================================================
	/*

		var record_limit int = 100000
		var serial_number int
		for serial_number = 0; serial_number < record_limit; serial_number++ {
			tags["uniq"] = strconv.Itoa(fd) + "_" + strconv.Itoa(row_number) + "_" + strconv.Itoa(serial_number)
			eventTime = time.Now() //.Add(time.Second * -20)
			check_suc, check_exist, _ := connection.Check_Record_Exist(table_name, eventTime, tags, fields)
			if !check_suc || !check_exist {
				break
			}
		}

		if serial_number >= record_limit {
			success = false
			return success, fmt.Errorf("records exceed limit %v at: eventTime: %v, fd: %v, row_number: %v", record_limit, eventTime, fd, row_number)
		}
	*/
	//====================================
	/*
		var eventTime time.Time
		eventTime = time.Now()
		tags["uniq"] = strconv.Itoa(fd) + "_" + strconv.Itoa(row_number)
	*/
	//=================================================
	
	
}


func (connection *InfluxDB_Client_struct) Create_database() (bool, error) {

	//===========================================================
	create_success, _, err := connection.InfluxDB_Query(fmt.Sprintf("create database %s", connection.Workspace))

	return create_success, err

}

func (connection *InfluxDB_Client_struct) Check_Record_Exist(table_name string, eventTime time.Time, tags map[string]string, fields map[string]interface{}) (bool, bool, error) {

	var err error
	
	var where_and string = " where"

	sql_string := "select" + " count(*) from " + table_name

	sql_string = sql_string + " " + where_and + " time = " + strconv.FormatInt(eventTime.Unix(), 10) + "000000000"
	where_and = "and"

	for tag_name, tag_value := range tags {
		sql_string = sql_string + " " + where_and + " " + tag_name + " = '" + tag_value + "'"
		where_and = "and"
	}

	for fields_name, fields_value := range fields {
		sql_string = sql_string + " " + where_and + " " + fields_name + " = '" + fields_value.(string) + "'"
		where_and = "and"
	}
	sql_string = sql_string + ";"

//	var Query_success bool
//	var response []client.Result
//	var Query_err error

	Query_success, response, Query_err := connection.InfluxDB_Query(sql_string)
	if !Query_success {
		return false, false, fmt.Errorf("Check_Record_Exist failed, Query_err: %v", Query_err)
	}

	success, _, rows_value := Resolve_InfluxDB_return(response)
	if !success {
		return false, false, fmt.Errorf("Check_Record_Exist failed")
	}

	exist := false
	if rows_value != nil {

		exist = true
	}

	return success, exist, err
}

func (connection *InfluxDB_Client_struct) InfluxDB_Query(Query_String string) (bool, []client.Result, error) {

	q := client.Query{
		Command:  fmt.Sprintf(Query_String),
		Database: connection.Workspace,
	}
	resp, err := connection.InfluxDB_Client.Query(q)
	if err != nil {
		return false, nil, fmt.Errorf("InfluxDB_Query, Query fail: %s: %v", Query_String, err)
	}
	if resp.Error() != nil {
		return false, nil, fmt.Errorf("InfluxDB_Query, resp Error: %s: resp: %v", Query_String, resp)
	}

	return true, resp.Results, err
}

func Resolve_InfluxDB_return(input []client.Result) (success bool, field_name []string, rows_value [][]interface{}) {
	success = false
	if input != nil && len(input) > 0 {
		if len(input[0].Series) > 0 {
			field_name = input[0].Series[0].Columns
			rows_value = input[0].Series[0].Values
			success = true
		}

	}
	return success, field_name, rows_value
}



func (connection *InfluxDB_Client_struct) get_total_count(table_name string) error {

	var sql_string string

//	sql_string = "use " + connection.Workspace
//	fmt.Println(sql_string)
//	Query_success, response, Query_err := connection.InfluxDB_Query(sql_string)
//	if !Query_success {
//		fmt.Printf("get_total_count 1 failed, Query_err: %v\n", Query_err)
//		return fmt.Errorf("get_total_count 1 failed, Query_err: %v", Query_err)
//	}

	sql_string = "select" + " count(*) from " +  table_name
//	fmt.Println(sql_string)
	Query_success, response, Query_err := connection.InfluxDB_Query(sql_string)
	if !Query_success {
		fmt.Printf("get_total_count 2 failed, Query_err: %v\n", Query_err)
		return fmt.Errorf("get_total_count 2 failed, Query_err: %v", Query_err)
	}

	// rows_value [][]interface{}
//	success, field_name, rows_value := Resolve_InfluxDB_return(response)
	success, _, rows_value := Resolve_InfluxDB_return(response)
	if !success {
		fmt.Printf("get_total_count failed\n")
		return fmt.Errorf("get_total_count failed")
	}

	

	message := fmt.Sprintf("%v\trows_value\t%v\n", time.Now().UnixNano() / int64(time.Millisecond), rows_value)
//	fmt.Println(message)

	
 //   dataBytes := []byte(message)

    // Use WriteFile to create a file with byte data.
//	ioutil.WriteFile("StressTesting.txt", dataBytes, 0)

	f, err := os.OpenFile("StressTesting.txt", os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		panic(err)
	}

	defer f.Close()

	if _, err = f.WriteString(message); err != nil {
		panic(err)
	}

/*
	f, err := os.Open("StressTesting.txt")
    if err != nil {
        fmt.Println(err)
        return err
    }
    _, err = f.WriteString(message)
    if err != nil {
        fmt.Println(err)
        f.Close()
        return err
    }
    err = f.Close()
    if err != nil {
        fmt.Println(err)
        return err
	}*/
	return nil
}