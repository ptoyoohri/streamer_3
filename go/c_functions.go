package main
/*
#include <stdio.h>

int temp_file()
{
	int fd = -1;

	FILE *filepointer = NULL;
	filepointer = tmpfile(); 
	if(filepointer)
		fd = fileno(filepointer);

	return fd;
}
*/
import "C"


/*
	
	$$	因為 go_json2csv_streamer_3.go 已經有 export function 了( JsonToCSV() ), 而 extern 和 export 不能放在同一個 .go 檔中
	$$	否則 compiler 會出現 類似
	
		/tmp/go-build099883646/b001/_x002.o: In function `c_temp_file':
		./go_json2csv_streamer_3.go:7: multiple definition of `c_temp_file'
		/tmp/go-build099883646/b001/_x001.o:/tmp/go-build/go_json2csv_streamer_3.go:7: first defined here

	
	$$	但是 extern 分開放的話, 其他 .go 檔又無法直接以 C.<functionname> 來 call 
	$$	所以需要以下function, 目的是為其他 .go 檔程式提供窗口
*/
func C_temp_file() int { 
	fd := int(C.temp_file())
    
    return fd
}
