package main

import (
	"errors"
	"reflect"
	"strconv"
)

// $$ reference https://play.golang.org/p/kwc32A5mJf
// ?? 把version_struct一整個structure傳進來, 用reader讀一行然後寫進version_struct裡面
func CsvUnmarshal(record []string, v reflect.Value) (success bool, marshal_err error) {

	/*
		$$
	*/

	// $$ Leo: json讀進來的一行資料有標明每個欄位的 fieldname 是什麼
	// $$ , 但 csv 卻沒有, 是完全只能看順序, 所以 csv_config.json 裡 data_structure 的順序不能亂掉, 必須要跟 傳進來的資料 的順序一樣
	/* $$
	兩種情況:

	假設資料為 "A","B","C"
	"B" 我不想寫進BO, 那在json裡仍要定義 data structure, 但不要寫 _bo

	假設資料中 "B" 完全被刪掉, 變成 "A","C"
	那 json 的 data structure 也不可以定義 B (只有不寫 _bo 是不夠的, 要整個 data structure 刪掉)

	以上兩種情況, 在寫進 table 時, B 的欄位都會留白
	*/
	success = true
	var temp_err_string string

	//	if v.NumField() != len(record) {
	//		temp_err_string = "data structure number does not match csv table column number " + strconv.Itoa(v.NumField()) + " != " + strconv.Itoa(len(record))
	//		marshal_err = errors.New(temp_err_string)
	//		success = false
	//		return success, marshal_err
	//	}

	// $$ v 是所有 json 中有定義 data structure 的欄位, 不論是否有 _bo
	for i := 0; i < v.NumField() && i < len(record); i++ {
		//		fmt.Println("record[", i, "]: ", record[i])
		f := v.Field(i)
		switch f.Type().String() {
		case "string":
			f.SetString(record[i])
		case "int":
			ival, err := strconv.ParseInt(record[i], 10, 0)
			if err != nil {
				temp_err_string = "csv unmarshall parse int fail: " + err.Error()
				marshal_err = errors.New(temp_err_string)
				//	success = false
			}
			f.SetInt(ival)
		default:
			f.Set(reflect.ValueOf(record[i]))
		}
	}
	return success, marshal_err
}
