﻿package main

//go build -buildmode=plugin functions.go

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"time"
)

var (
	funcs      = map[string]func(record map[string]string, input string, output_parse map[string]interface{}) (output interface{}, success bool, err error){"Datetime2Timestamp": Datetime2Timestamp, "DatetimeFormat": DatetimeFormat, "Regex": Regex, "Value": Value}
	CacheRegex map[string]interface{}
)

/*
stdLongMonth      = "January"
stdMonth          = "Jan"
stdNumMonth       = "1"
stdZeroMonth      = "01"
stdLongWeekDay    = "Monday"
stdWeekDay        = "Mon"
stdDay            = "2"
stdUnderDay       = "_2"
stdZeroDay        = "02"
stdHour           = "15"
stdHour12         = "3"
stdZeroHour12     = "03"
stdMinute         = "4"
stdZeroMinute     = "04"
stdSecond         = "5"
stdZeroSecond     = "05"
stdLongYear       = "2006"
stdYear           = "06"
stdPM             = "PM"
stdpm             = "pm"
stdTZ             = "MST"
stdISO8601TZ      = "Z0700"  // prints Z for UTC
stdISO8601ColonTZ = "Z07:00" // prints Z for UTC
stdNumTZ          = "-0700"  // always numeric
stdNumShortTZ     = "-07"    // always numeric
stdNumColonTZ     = "-07:00" // always numeric
*/

/*
func get_test_1(record map[string]string) (bool, error) {

	var row_success bool = true
	var err error

	key := "test_1"

	var exist bool
	var test_2 string
	var test_3 string

	if test_2, exist = Parse_result_cache["test_2"].(string); !exist {
		if row_success, err = get_test_2(record); row_success {
			test_2 = Parse_result_cache["test_2"].(string)
		}
	}

	if test_3, exist = Parse_result_cache["test_3"].(string); !exist {
		if row_success, err = get_test_3(record); row_success {
			test_3 = Parse_result_cache["test_3"].(string)
		}
	}

	Parse_result_cache[key] = test_2 + " " + test_3

	fmt.Println("in get_test_1: ", Parse_result_cache[key])

	return row_success, err
}
*/
func Test_1(records map[string]map[string]string, target_table string, target_key string, bo_fields_map_ver map[string][]map[string]interface{}, boColOrder_ver map[string]map[int]int, boCol map[string]map[string]int, Parse_result_cache map[string]map[string]string) (interface{}, map[string]map[string]string, bool, error) {
	// $$ record: 整筆record
	// $$ input: 單一目標欄位
	// $$ output_parse: _parse整個struct, 此function需要的parameter也從這裡拿. ex: output_parse["parameter_name"]
	/*
		fmt.Println("in Test_1")
		row_success = true
		var exist bool
		if output, exist = Parse_result_cache["test_1"]; !exist {
			if row_success, err = get_test_1(record); row_success {
				output = Parse_result_cache["test_1"]
			}
		}
		return output, row_success, err
	*/

	/*
		records map[string]map[string]string
		, target_table string
		, target_key string
		, bo_fields_map_ver map[string][]BO_FIELD
		, boColOrder_ver map[string]map[int]int
		, boCol map[string]map[string]int
		, Parse_result_cache map[string]map[string]string
	*/
	fmt.Println("in Test_1")

	var output interface{}
	row_success := true
	var err error
	var exist bool

	//	var test_2 string
	//	var test_3 string

	if output, exist = Parse_result_cache[target_table][target_key]; !exist {
		//	if row_success, err = get_test_1(record); row_success {
		//		output = Parse_result_cache["test_1"].(string)
		//	}
		var test_2 interface{}
		var test_3 interface{}
		var test_5 interface{}

		if test_2, exist = Parse_result_cache[target_table]["test_2"]; !exist {
			if test_2, Parse_result_cache, row_success, err = Test_2(records, target_table, "test_2", bo_fields_map_ver, boColOrder_ver, boCol, Parse_result_cache); row_success {
				test_2 = Parse_result_cache[target_table]["test_2"]
			} else {
				return output, Parse_result_cache, row_success, err
			}
		}

		if test_3, exist = Parse_result_cache[target_table]["test_3"]; !exist {
			if test_3, Parse_result_cache, row_success, err = Test_3(records, target_table, "test_3", bo_fields_map_ver, boColOrder_ver, boCol, Parse_result_cache); row_success {
				test_3 = Parse_result_cache[target_table]["test_3"]
			} else {
				return output, Parse_result_cache, row_success, err
			}
		}

		if test_5, exist = Parse_result_cache[target_table]["test_5"]; !exist {
			if test_5, Parse_result_cache, row_success, err = Test_5(records, target_table, "test_5", bo_fields_map_ver, boColOrder_ver, boCol, Parse_result_cache); row_success {
				test_5 = Parse_result_cache[target_table]["test_5"]
			} else {
				return output, Parse_result_cache, row_success, err
			}
		}

		output = records[target_table][target_key] + "," + test_2.(string) + "," + test_3.(string) + "," + test_5.(string)
		Parse_result_cache[target_table][target_key] = output.(string)

	}

	return output, Parse_result_cache, row_success, err
}

/*
func get_test_2(record map[string]string) (bool, error) {

	var row_success bool = true
	var err error

	key := "test_2"

	Parse_result_cache[key] = record[key]
	fmt.Println("in get_test_2: ", Parse_result_cache[key])

	return row_success, err
}
*/
func Test_2(records map[string]map[string]string, target_table string, target_key string, bo_fields_map_ver map[string][]map[string]interface{}, boColOrder_ver map[string]map[int]int, boCol map[string]map[string]int, Parse_result_cache map[string]map[string]string) (interface{}, map[string]map[string]string, bool, error) {
	// $$ record: 整筆record
	// $$ input: 單一目標欄位
	// $$ output_parse: _parse整個struct, 此function需要的parameter也從這裡拿. ex: output_parse["parameter_name"]
	fmt.Println("in Test_2")
	var output interface{}
	row_success := true
	var err error
	var exist bool

	if output, exist = Parse_result_cache[target_table][target_key]; !exist {

		order_in_table_setting := boCol[target_table][target_key]
		order_in_bo_struct := boColOrder_ver[target_table][order_in_table_setting]

		parameter := bo_fields_map_ver[target_table][order_in_bo_struct]["Parse"].(map[string]interface{})["parameter"].(string)

		output = records[target_table][target_key] + "~" + parameter
		Parse_result_cache[target_table][target_key] = output.(string)

	}
	return output, Parse_result_cache, row_success, err
}

/*
func get_test_3(record map[string]string) (bool, error) {

	var row_success bool = true
	var err error

	key := "test_3"

	Parse_result_cache[key] = record[key]
	fmt.Println("in get_test_3: ", Parse_result_cache[key])

	return row_success, err
}
*/
func Test_3(records map[string]map[string]string, target_table string, target_key string, bo_fields_map_ver map[string][]map[string]interface{}, boColOrder_ver map[string]map[int]int, boCol map[string]map[string]int, Parse_result_cache map[string]map[string]string) (interface{}, map[string]map[string]string, bool, error) {
	// $$ record: 整筆record
	// $$ input: 單一目標欄位
	// $$ output_parse: _parse整個struct, 此function需要的parameter也從這裡拿. ex: output_parse["parameter_name"]
	fmt.Println("in Test_3")
	var output interface{}
	row_success := true
	var err error
	var exist bool

	if output, exist = Parse_result_cache[target_table][target_key]; !exist {

		order_in_table_setting := boCol[target_table][target_key]
		order_in_bo_struct := boColOrder_ver[target_table][order_in_table_setting]

		parameter := bo_fields_map_ver[target_table][order_in_bo_struct]["Parse"].(map[string]interface{})["parameter"].(string)

		output = records[target_table][target_key] + "~" + parameter
		Parse_result_cache[target_table][target_key] = output.(string)

	}
	return output, Parse_result_cache, row_success, err
}

func Test_5(records map[string]map[string]string, target_table string, target_key string, bo_fields_map_ver map[string][]map[string]interface{}, boColOrder_ver map[string]map[int]int, boCol map[string]map[string]int, Parse_result_cache map[string]map[string]string) (interface{}, map[string]map[string]string, bool, error) {
	// $$ record: 整筆record
	// $$ input: 單一目標欄位
	// $$ output_parse: _parse整個struct, 此function需要的parameter也從這裡拿. ex: output_parse["parameter_name"]
	fmt.Println("in Test_5")
	var output interface{}
	row_success := true
	var err error
	var exist bool

	if output, exist = Parse_result_cache[target_table][target_key]; !exist {

		output = records[target_table][target_key]
		Parse_result_cache[target_table][target_key] = output.(string)

	}
	return output, Parse_result_cache, row_success, err
}

//===================================================================

func Datetime2Timestamp(record map[string]string, input string, output_parse map[string]interface{}) (output interface{}, success bool, err error) {

	//get input datetime format
	success = true
	tz, err := strconv.Atoi(output_parse["timezone"].(string))
	if err != nil {
		fmt.Println(err)
		return nil, success, err
	}

	//calculate timezone difference
	hr := fmt.Sprintf("%dh", tz*-1)
	tzdiff, err := time.ParseDuration(hr)
	if err != nil {
		fmt.Println(err)
		return nil, success, err
	}

	//calculate local datetime
	datetime, err := time.Parse(output_parse["dateformat"].(string), input)
	if err != nil {
		fmt.Println(err)
		return nil, success, err
	}
	datetime = datetime.Add(tzdiff)

	//return timestamp
	return strconv.FormatInt(int64(datetime.Unix()), 10), success, err
}

func Test_leo_func(record map[string]string, input string, output_parse map[string]interface{}) (output interface{}, success bool, err error) {

	//get input datetime format
	success = true
	divide, err1 := strconv.Atoi(output_parse["divide"].(string))
	temp_input, err2 := strconv.Atoi(input)

	if err1 == nil && err2 == nil {
		output = temp_input / divide
	}
	fmt.Println("in test_leo_func ", temp_input, " / ", divide, " = ", output)
	//return output datetime format
	return output, success, err1
}

func Test_error(record map[string]string, input string, output_parse map[string]interface{}) (output interface{}, success bool, err error) {

	success = true
	add_string, _ := output_parse["add_string"].(string)
	output = add_string
	fmt.Println("in test_error: ", output.(string))
	err = errors.New("sssaaa")
	return output, success, err
}

func DatetimeFormat(record map[string]string, input string, output_parse map[string]interface{}) (output interface{}, success bool, err error) {

	success = true
	//get input datetime format
	datetime, err := time.Parse(output_parse["dateformat"].(string), input)
	if err != nil {
		fmt.Println(err)
	}

	//return output datetime format
	return datetime.Format(output_parse["format"].(string)), success, err
}

func Regex(record map[string]string, input string, output_parse map[string]interface{}) (output interface{}, success bool, err error) {

	success = true
	//get output regex position
	pos, _ := strconv.Atoi(output_parse["pos"].(string))

	//if regex cache exists
	if len(CacheRegex) > 0 && len(CacheRegex) < 10000 {
		if _, ok := CacheRegex[input]; ok {
			if CacheRegex[input].(map[string]interface{})["match"] == output_parse["match"].(string) {
				return CacheRegex[input].(map[string]interface{})["result"].(map[int]interface{})[pos].(interface{}), success, err
			}
		}
	} else { //clear cache memory when stored too many records
		CacheRegex = nil
	}

	//get regex formula
	re := regexp.MustCompile(output_parse["match"].(string))

	//find all match words
	match := re.FindStringSubmatch(input)
	if len(match) < pos+1 {
		return "", success, err
	}
	//fmt.Println("match:",match , "pos",pos, "output",match[pos])

	//create regex cache
	resulmap := make(map[int]interface{})
	for i, _ := range match {
		resulmap[i] = match[i]
		mp := map[string]interface{}{
			"match":  output_parse["match"].(string),
			"result": resulmap,
		}
		CacheRegex = map[string]interface{}{
			input: mp,
		}
	}

	return match[pos], success, err

}

func CheckCases(record map[string]string, input string, output_parse map[string]interface{}) (output interface{}, success bool, err error) {

	success = true
	//get output all cases
	cases := output_parse["cases"].([]interface{})

	for _, v := range cases {

		casecheck := true
		casemap := v.(map[string]interface{})

		if casemap["condition_col"] != nil {
			colary := make([]string, len(casemap["condition_col"].([]interface{})))
			valary := make([]string, len(casemap["condition_val"].([]interface{})))

			for i, v := range v.(map[string]interface{})["condition_col"].([]interface{}) {
				colary[i] = fmt.Sprint(v)
			}

			for i, v := range v.(map[string]interface{})["condition_val"].([]interface{}) {
				valary[i] = fmt.Sprint(v)
				// fmt.Println("arrrr:",valary[i],record[colary[i]])
				if record[colary[i]] != valary[i] {
					casecheck = false
				}
			}
		}

		if casecheck || casemap["condition_col"] == nil {
			FuncName := casemap["func"].(string)

			a := funcs[FuncName]
			output, success, err = a(record, input, casemap)
			//fmt.Println(output)
			if output != "" {
				break
			}

		}

	}

	return output, success, err

}

func Value(record map[string]string, input string, output_parse map[string]interface{}) (output interface{}, success bool, err error) {

	if output_parse["input"] != nil {
		output = record[output_parse["input"].(string)]
	} else {
		output = output_parse["value"].(string)
	}

	return output, success, err
}

//=======================================

func print_err_msg(input_error error, input_msg string) error {
	var err error
	var temp string
	if input_error != nil {
		temp = input_error.Error()
	}
	if input_msg != "" {
		if temp != "" {
			temp = temp + ": "
		}
		temp = temp + input_msg
	}
	fmt.Println(temp)
	err = errors.New(temp)
	return err
}
